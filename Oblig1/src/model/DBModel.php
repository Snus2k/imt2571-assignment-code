<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
            try{

                $this->db = new PDO('mysql:host=mysql.ansatt.ntnu.no; dbname =simenbje_DMDB; charset=utf8mb4','simenbje','bjerksim');
                $this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);


            }
            catch(PDOException $e){
                echo $e->getMessage();
            }
		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
       public function getBookList()
 	{
		 $booklist = array();
		try
		{
            foreach($this->db->query('SELECT * FROM simenbje_DMDB.book') as $row) {
			 $booklist[] = new Book($row['Title'], $row['Author'], $row['Description'], $row['id'] );
		 }


		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

        return $booklist;

	 }




    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        if($id != ' ' &&  $id != NULL){
            try{

                $stmt=$this->db->prepare("SELECT * FROM simenbje_DMDB.book WHERE id=:id");

                $stmt->execute(array(':id'=> $id));
                $row = $stmt->fetch(PDO::FETCH_ASSOC);


                $book = new Book($row['Title'], $row['Author'], $row['Description'], $row['id'] );

                return $book;

            }
            catch(PDOException $e){

                echo $e->getMessage();
            }

        }
        else{
            echo "Invalid entry ID";
            die();
        }


    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {

        if($book->title != ' '&& $book->title != NULL && $book->author != ' '&& $book->author != NULL){


            try{
                $stmt = $this->db->prepare("INSERT INTO simenbje_DMDB.book(Title, Author, Description) VALUES(:Title, :Author, :Description)");

                $stmt->execute(array(

                    ':Title' => $book->title,
                    ':Author' => $book->author,
                    ':Description' => $book->description
                    ));
            }

            catch(PDOException $e){
                echo $e->getMessage();
            }

        }

        else{

            echo "Title or Author is empty";
        }
    }
    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        if($id != ' ' &&  $id != NULL){
            if($book->title != ' '&& $book->title != NULL && $book->author != ' '&& $book->author != NULL){

                try{

                    $stmt = $this->db->prepare("UPDATE simenbje_DMDB.book SET Title = :title, Author = :author, Description = :desc WHERE id=:id");

                    $stmt->execute(array(
                        ':id' => $book->id,
                        ':title' => $book->title,
                        ':author' => $book->author,
                        ':desc' => $book->description
                        ));

                    return $book;
                }

                catch(PDOException $e){

                    echo $e->getMessage();

                }
            }

            else{

                echo "Title or Author is empty";
                die();
            }
        }
        else{
            
            echo "Invalid ID";
            die();

        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {

        if($id != ' ' &&  $id != NULL){
            try{
                $stmt = $this->db->prepare("DELETE FROM simenbje_DMDB.book WHERE id=:id");

                $stmt->execute(array(
                    ':id'=>$id
                    ));
            }

            catch(PDOException $e){

                echo $e->getMessage();

            }
        }
        else{
            echo "Invalid ID";
            die();
        }
    }

}

?>